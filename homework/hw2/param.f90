!! Module for setting paramater of pi
module param

!! Implicit variables   
	implicit none
   
!! Create value for pi, real number
    real(kind=SELECTED_REAL_KIND(18,34)), parameter :: pi = acos(-1.0)
    
 contains

end module param


