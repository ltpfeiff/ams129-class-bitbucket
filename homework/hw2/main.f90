!! Main program for Homework 2
program main

!! Implicit variables
    implicit none

!! Create variable for the threshold value to be entered
   real(kind=SELECTED_REAL_KIND(18,34)) :: threshold  

!! Read the user-entered threshold
    print*, "Enter value:"
    read*, threshold


!! Call the subroutine file for user input
   call calc_base1(threshold)


!!End rogram for Homework 2
end program main
