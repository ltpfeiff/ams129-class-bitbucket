!! Subroutine for Homework 2
subroutine calc_base1(threshold)

!! Use the parameter file for the value of pi
    use param, only:pi

!! Implicit variables
    implicit none

!! Define all variables and the actal solution to the equation
    
    real(kind=SELECTED_REAL_KIND(18,34)) :: threshold, solution, error, real_soln, iter
    solution = (pi**2/6)                                     !! This is the true solution to the equation
    real_soln = 0.0                                          !! Start real solution at 0 and build up
    iter = 1.0                                               !! Start counter at 1

!! Define error value big enough
    error = 10000.0
     

!! Infinite summation do-while loop

    Do while(error > threshold)
       real_soln = real_soln + (1.0/(iter**2.0))             !! Equation for Basel problem 
       if (MOD(iter , 1000.0) == 0) then                     !! If the remainder is zero, then...
!! Print out approximation of pi and the error 
        print*, "n = ", iter
	print*, "error = ", error
       end if                                                !! End the if once it prints
        iter = iter + 1                                      !! Increase iter by 1 each time
       error = ABS(real_soln - solution)                     !! Error is the difference
     end do 
     print*, "Cycles = ", iter
     print*, "Solution = ", real_soln
     print*, "Error = ", error


!! Call and write to results section
 
    open(20, file='results.txt')
    write(20,*) real_soln
    write(20,*) error
    close(20)


!! End the subroutine
end subroutine calc_base1
