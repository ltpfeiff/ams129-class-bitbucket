## HW1
[10/10] Well done!

## HW2
[10/10]
- Well done!
- I fixed your makefile specified with your codes. BTW, Basel is the name of mathematician, not base"1".
- Indentation for "makefile" is a "tab character" not spaces.

## HW3
[10/10]
- Well done!
- Missing url.txt file. But I seen that you uploaded the files on the server side, so it's OK.

## HW4
[7/10]
- Well, I guess you were almost there. You have every ingredient in your code to finish the HW4 properly.
- Good luck to your final project!
