# The Python part of the Project


# Setup/Import functions
import numpy as np
import matplotlib.pyplot as plt
import math
plt.interactive(False)



# Path to Fortran Folder and Run the Code
import os
os.getcwd()
path = '../'
os.chdir(path)
os.listdir(os.curdir)
path = ('./fortran')
os.chdir(path)
os.getcwd()
file_path = ('./output.txt')


# Numpy Array
numsoln = np.loadtxt(file_path)[:,1]
print(numsoln)


# Calculate Real Solution through a list
List=[]
def realsoln():
   for x in np.arange(0, 10.25, .25):
     y = -((2*(np.log((x**2)+1))+4)**.5)
     List.append(y)
   return List
print(realsoln())



# Calculate Error
error = sum(abs(List - numsoln))
print(error)

# Make x value range
x = np.arange(0, 10.25, .25)

# Numerical Solution Graph with Red Dashed Lines
plt.plot(x, numsoln, 'ro--')

# Real Solution Graph with Blue solid line
plt.plot(x, List, 'b-')


# Graph Details
plt.xlabel('Increments')
plt.ylabel('Value')
plt.title(error)
plt.grid(True)
plt.legend(('numsoln', 'realsoln'), loc='upper right')

# Show results on screen
plt.show()


# Save file as png file
plt.savefig("finalproject.png")

