! Function Module

!! MayMay helped me with some of this code in the beginning at section on Tuesday

module func

! Set implicit variables
  implicit none

  real:: x, y, h  
  contains 


! Subroutine
   real function f(x,y)
    implicit none  
    real, intent(in) :: x, y

! Equation
    f = (2.00*x)/(y*(1.00*x**2))

! End subroutine
  end function 


! End module
end module func
