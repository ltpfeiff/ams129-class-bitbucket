
program Euler

! Use module func
 use func 

! Implicit Variables
 implicit none

! Define variable
 integer :: i

! Set variables
  h = 0.25
  x = 0.00
  y = -2.00
  open(unit = 10, file = 'output.txt')
  print *, x,  y
  write(10,*) x, y


! Do Loop
 do i = 1, 40
!    print *, x , y
!    write(10,*) x, y
  x = x+h
     print *, x, y
     write(10,*) x, y
  y = y + (h*((2.00*x)/(y*(1.00+(x**2)))))
 end do

close(10)


! End Program
end program euler


